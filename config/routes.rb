Undevise::Engine.routes.draw do
  	root :to => 'auth#index'
  	match ':provider/callback' => 'auth#callback'
end
