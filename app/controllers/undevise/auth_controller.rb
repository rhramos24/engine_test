require_dependency "undevise/application_controller"

module Undevise
  class AuthController < ApplicationController
  	def index
      render :text => 'Hello world'
    end

    def callback
      render :text => "Hello from #{params[:provider]}"
    end
    
  end
end
