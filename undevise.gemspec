$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "undevise/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "undevise"
  s.version     = Undevise::VERSION
  s.authors     = ["Humberto Ramos"]
  s.email       = ["rhramos24@gmail.com "]
  s.homepage    = "http://www.prueba.ramos.com/"
  s.summary     = "Summary of Undevise."
  s.description = "Description of Undevise."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.19"
  # s.add_dependency "jquery-rails"
  s.add_dependency "omniauth"
  s.add_dependency "omniauth-twitter"
  s.add_dependency "omniauth-facebook"

  s.add_development_dependency "sqlite3"
end
